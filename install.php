<?php

include 'var.php';

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$passwd = $GLOBALS['pass'];
$dbname = $GLOBALS['dbname'];
$game = $GLOBALS['game'];

$link = mysql_connect($servername, $username, $passwd);
if (!$link) {
	die('Connexion impossible : ' . mysql_error());
}
$sql = 'CREATE DATABASE '.$dbname.'';
mysql_query($sql, $link);

$sql = 'CREATE DATABASE '.$game.'';
mysql_query($sql, $link);

mysqli_close($link);

//User database

$conn = mysqli_connect($servername, $username, $passwd, $game);
if (!$conn)
	die("Connection failed: " . mysqli_connect_error());

$sql_game = "CREATE TABLE IF NOT EXISTS ".$GLOBALS['party_name']." (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	name VARCHAR(30) NOT NULL,
	mode VARCHAR(30) NOT NULL,
	value INT(6) UNSIGNED NOT NULL,
	nb_joueur_max INT(6) UNSIGNED NOT NULL,
	nb_joueur INT(6) UNSIGNED NOT NULL
	)";

mysqli_query($conn, $sql_game);

mysqli_close($conn);

//table party

$conn = mysqli_connect($servername, $username, $passwd, $dbname);
if (!$conn)
	die("Connection failed: " . mysqli_connect_error());
$sql_users = "CREATE TABLE IF NOT EXISTS ".$GLOBALS['guest']." (
	id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
	firstname VARCHAR(30) NOT NULL,
	lastname VARCHAR(30) NOT NULL,
	login VARCHAR(30) NOT NULL,
	passwd TEXT NOT NULL,
	member VARCHAR(30) NOT NULL,
	rang VARCHAR(30) NOT NULL,
	name_game VARCHAR(30) NOT NULL,
	team VARCHAR(30) NOT NULL,
	points INT(5) UNSIGNED NOT NULL
	)";

mysqli_query($conn, $sql_users);

$admin_p = hash("whirlpool", 'admin');
$count = mysqli_query($conn,"SELECT * FROM ".$GLOBALS['guest']." WHERE login='admin'");
if (!mysqli_num_rows($count))
	mysqli_query($conn, "INSERT INTO ".$GLOBALS['guest']." (`id`, `firstname`, `lastname`, `login`, `passwd`, `member`, `rang`, `name_game`, `team`, `points`) VALUES (NULL, 'admin','admin','admin','".$admin_p."', 'admin', 'noob', 'none', 'none', '0')");

mysqli_close($conn);

?>
