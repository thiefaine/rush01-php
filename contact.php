<?php
session_start();

include 'install.php';
?>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="rush.css" />
	<title>42</title>
</head>
<body>
<?php
include 'head.php';
?>
	<div id="contact">
		<form action="reponse.php" method="post">
			<h3>Envoyez un message</h3>
			<p>
				<label for="id_contact">Objet</label>
				<select name="id_contact">
					<option value="0">-- Choisir --</option>
					<option value="2" >Service client</option>
					<option value="1" >Webmaster</option>
				</select>
			</p>
			<p>
				<label for="login">Votre login</label>
				<input type="text" name="from" value="" required="required" />
			</p>
			<label for="message">Message</label>
			<p>
				<textarea name="message" rows="15" cols="20" style="width:340px;height:220px" required="required" ></textarea>
			</p>
			<p>
				<input type="submit" name="submitMessage" value="Envoyer" class="button_large" />
			</p>
		</form>
	</div>
<?php
include 'footer.php'
?>

</body>
</html>
