<?php
session_start();

include 'var.php';
include 'install.php';

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$passwd = $GLOBALS['pass'];
$dbname = $GLOBALS['dbname'];

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="rush.css" />
	<title>42</title>
</head>
<body>

<?php
include 'head.php';

function captcha()
{
	if(IsSet($_POST['verif_code']) AND !Empty($_POST['verif_code'])) 
	{
		echo $_POST['verif_code'];
		echo "1</br >";
		echo $_SESSION['aleat_nbr'];
		echo "2</br >";
		if($_POST['verif_code'] == $_SESSION['aleat_nbr']) 
			return TRUE;
		echo "<p class='error'>Merci de reessayer</p>";
		return FALSE;
	}
	return FALSE;
}

if ($_POST['login'] and $_POST['passwd'] and $_POST['firstname'] and $_POST['lastname'])
{
	$login = htmlspecialchars($_POST['login']);
	$password = hash("whirlpool", htmlspecialchars($_POST['passwd']));
	$lastname = htmlspecialchars($_POST['lastname']);
	$firstname = htmlspecialchars($_POST['firstname']);

	$conn = mysqli_connect($servername, $username, $passwd, $dbname);
	if (!$conn)
		die("Connection failed: " . mysqli_connect_error());
	$count = mysqli_query($conn,"SELECT * FROM ".$GLOBALS['guest']." WHERE login='".$login."'");
	if (!mysqli_num_rows($count))
	{
		if (captcha() == TRUE)
		{
			mysqli_query($conn, "INSERT INTO ".$GLOBALS['guest']." (`id`, `firstname`, `lastname`, `login`, `passwd`, `member`, `rang`, `name_game`, `team`, `points`) VALUES (NULL, '".$firstname."','".$lastname."','".$login."','".$password."', 'user', 'noob', 'none', 'none', '0')");
			header('Location: login.php');
		}
	}
	else
		echo "<p class='error'>Login déja existant</p>";
	mysqli_close($conn);
}

?>
	<form class="login" action="create.php" method="post">
		<label for='firstname'>Prenom : </label><input id='firstname' type="text" name="firstname" value="" required="required" />
		<br />
		<label for='lastname'>Nom : </label><input id='lastname' type="text" name="lastname" value="" required="required" />
		<br />
		<label for='login'>Identifiant : </label><input id='login' type="text" name="login" value="" required="required" />
		<br />
		<label for='passwd'>Mot de passe : </label><input id='passwd' type="password" name="passwd" value="" required="required" />
		<br />
		<p><img id='code_gen' src="verif_code_gen.php" alt="Code de vérification" /></p>
		<label for='captcha'>Merci de retaper le code de l'image : </label><input type="text" name="verif_code" required="required" />
		<br />
		<br />
		<input class='sub' type="submit" name="submit" value="OK" />
		<br />
	</form>
<?php
include 'footer.php'
?>
</body></html>
