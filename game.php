<?php

session_start();

function ThrowDices()
{
	return (rand(1, 6));
}

require_once 'Destroyer.class.php';
require_once 'Fregate.class.php';
require_once 'Cuirrasier.class.php';

include('var.php');

if ($_SESSION['partie'])
{

	?>

	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="UTF-8">
		<title>Game - 42</title>
		<link rel='stylesheet' src='rush.css'>
	</head>
	<body>
		<?php

		$name = $_SESSION['partie'];

		$servername = $GLOBALS['server'];
		$username = $GLOBALS['user'];
		$passwd = $GLOBALS['pass'];
		$game = $GLOBALS['game'];
		$party_name = $GLOBALS['party_name'];

		$conn = mysqli_connect($servername, $username, $passwd, $game);
		if (!$conn)
			die('Connection failed');
		$res = mysqli_query($conn, "SELECT * FROM game_".$name."");
		$row = mysqli_fetch_array($res);

		$turn = $row['turn'];
		$action = $row['action'];

		$id_player = $row[$turn.'_player'];

		echo "--".$name."--<br />";
		echo "Tour : player ".$turn."<br />";
		echo "Action : ".$action."<br />";

		$_SESSION['i_ship'] = 0;
		if ($_SESSION['loggued_on_user'] == $id_player)
		{
		//recuperation des donnes des vaisseeaux pour afficahge et modification
		//ajout de notre vaisseau dans une variable de session
			$un = $row[$turn."_player_ship"];
			$objs = unserialize($un);

			echo "<p>A ton tour de jouer</p>";	
			print($objs[$_SESSION['i_ship']]);

			if ($action == 'order')
			{
				?>

				<form action='game_action_order.php' method='post'>
					<p>Order : </p>
					<label for='speed'>Speed</label><input type='text' id='speed' name='speed'><br />
					<label for='shield'>Shield</label><input type='text' id='shield' name='shield'><br />
					<label for='weapon'>Weapon</label><input type='text' id='weapon' name='weapon'><br />
					<?php
					echo "<input type='hidden' name='turn' value='".$turn."'>";
					?>
					<input type='submit' value='Attribute'>
				</form>

				<?php
			}
			else if ($action == 'move')
			{

				?>

				<form action='game_action_move.php' method='post'>
					<label>Go : </label>
					<select name='nb_cases'>
						<?php

						for ($i = 0; $i <= ($objs[$_SESSION['i_ship']]->getSpeed() + $objs[$_SESSION['i_ship']]->getPP_speed()); $i++)
						{
							echo "<option>".$i."</option>";
						}
						?>
					</select> cases<br />
					<?php
					echo "<input type='hidden' name='turn' value='".$turn."'>";
					?>
					<input type='submit' value='Move !!!' />
				</form>

				<?php
			}
			else if ($action == 'shoot')
			{
				?>

				<?php
			}
					//update du turn pour le suivant

		}
		else
		{
			echo "En attente de ton tour ...";
		}
		?>
		<iframe src='board.php' width='500' height='750'></iframe>



	</body>
	</html>

	<?php
}
else
	header('Location: ../index.php');

?>
