<?php

session_start();

include 'var.php';

require_once 'Destroyer.class.php';
require_once 'Fregate.class.php';
require_once 'Cuirrasier.class.php';

$name = $_SESSION['partie'];

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$passwd = $GLOBALS['pass'];
$game = $GLOBALS['game'];
$party_name = $GLOBALS['party_name'];

$conn = mysqli_connect($servername, $username, $passwd, $game);
if (!$conn)
	die('Connection failed');
$nb = mysqli_query($conn, "SELECT * FROM party_name WHERE name='".$_SESSION['partie']."'");
$nb_row = mysqli_fetch_array($nb);
$nb_max = $nb_row['nb_joueur_max'];

$res = mysqli_query($conn, "SELECT * FROM game_".$_SESSION['partie']."");
$row = mysqli_fetch_array($res);

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset='utf-8'>
	<title>42</title>
	<link rel='stylesheet' src='rush.css'>
</head>
<body>
	<?php
	for ($i = 1; $i <= $nb_max; $i++)
	{
		$color = $row[$i.'_player_team'];
		$objs = unserialize($row[$i.'_player_ship']);
		foreach($objs as $ship)
		{
			$or = $ship->getOrientation();
			$x = $ship->getX();
			$y = $ship->getY();
			$x_e = $x * 5;
			$y_e = $y * 5;
			if ($or == 1 or $or == 3)
			{
				$width = $ship->getW();
				$height = $ship->getH();
			}
			else
			{
				$width = $ship->getH();
				$height = $ship->getW();					
			}
			echo "<img src='img/".$i."_".$color.".png' style='min-width:".$width."px;min-height:".$height."px;position:absolute;bottom:".$y_e.";left:".	$x_e.";'>";
		}
	}
	?>
	<script>
	window.setInterval("reloadIFrame();", 3000);
		function reloadIFrame() {
			location.reload();
		}
	</script>
</body>
</html>