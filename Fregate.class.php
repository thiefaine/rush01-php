<?php

require_once('Weapon.class.php');
require_once('Doc.dump.php');
require_once('Lance.class.php');
require_once('Lance_navale_lourde.class.php');
require_once('Mitrailleues.class.php');
require_once('Macro_canon.class.php');
require_once('Batterie_laser.class.php');
require_once('Iship.interface.php');

Final class Fregate implements IShip
{
	const MAX_HEALTH = 5;
	const WIDTH = 1;
	const HEIGHT = 4;

	const MAX_PP = 10;
	const SPEED = 15;
	const MANIABILITY_INIT = 4;
	
	protected $health = self::MAX_HEALTH;
	protected $pp = self::MAX_PP;
	protected $orientation = 0;
	protected $posX = 0;
	protected $posY = 0;

	protected $maniability = 0;
	protected $pp_speed = 0;
	protected $pp_shield = 0;
	protected $pp_weapon = 0;

	protected $weapon = 0;

	use Doc;

		// orientation of the ship [0 => up, 1 => right, 2 => left, 3 => down]
	public $name = "Fregate";

	public function __construct($kwarg)
	{
		if (!$kwarg['weapon'])
			$this->weapon = new Batterie_laser();
		else
			$this->weapon = new $kwarg['weapon'];
		echo "Bulit a new Ship, its name is : test\n";
	}
	public function __toString()
	{
		$str = "<br />Points de Coque : " . $this->getHealth() . "<br />";
		$str = $str . "PP disponibles : " . $this->getPP() . "<br />";
        $str = $str . "Position X : ".$this->getX() . " / Y : ".$this->getY()."<br />";
		return $str;
	}

    //getters x y health
	public function getOrientation()
	{
		return $this->orientation;
	}

	public function getX()
	{
		return $this->posX;
	}

	public function getY()
	{
		return $this->posY;
	}
    public function getW()
    {
        return self::WIDTH;        
    }
    public function getH()
    {
        return self::HEIGHT;        
    }
	public function getHealth()
	{
		return $this->health;
	}

	public function getPP()
	{
		return $this->pp;
	}

	public function getSpeed()
	{
		return self::SPEED;
	}

	public function getManiability()
	{
		return $this->maniability;
	}

	public function getPP_speed()
	{
		return $this->pp_speed;
	}

	public function TakeDamage($degats)
	{
		if ($this->pp_shield > 0)
			$this->pp_shield -= $degats;
		else
			$this->health -= $degats;
		if ($this->health <= 0)
			$this->health = 0;
	}

    /*public function ArrayPos($nb_cases)
    {
        if ($this->orientation == 0)
        {
            $initY = $this->posY + ($this->height/2);
            $endY = $this->posY + ($this->height/2 + $nb_cases);
            return (array($initY, $endY));
        }
        elseif ($this->orientation == 1)
        {
            $initX = $this->posX + ($this->height/2);
            $endX = $this->posX + ($this->height/2 + $nb_cases);
            return (array($initX, $endX));
        }
        elseif ($this->orientation == 2)
        {
            $initY = $this->posY - ($this->height/2);
            $endY = $this->posY - ($this->height/2 + $nb_cases);
            return (array($initY, $endY));
        }
        elseif ($this->orientation == 3)
        {
            $initX = $this->posX - ($this->height/2);
            $endX = $this->posX - (($this->height/2) + $nb_cases);
            return (array($initX, $endX));
        }
    }*/

    public function Move_ship($nb_cases, $obstacles)
    {
    	if ($nb_cases > self::SPEED + $this->pp_speed) {
    		return -2;
            //you can't go so far
    	} else if ($nb_cases < $this->maniability) {
    		return -2;
            // must go at least farrther than $
    	} else if ($nb_cases < 0) {
    		return -2;
            //tu ne peux pas faire marche arriere
    	} else {
    		if ($this->orientation == 0 or $this->orientation == 2) {
    			$way = 1;
    			if ($this->orientation == 2)
    				$way = -1;

    			$x_i = $this->posX - intval(self::WIDTH / 2);
    			$y_i = $this->posY + (intval(self::HEIGHT / 2) * $way);

    			$x_e = $this->posX + intval(self::WIDTH / 2);
    			$y_e = $this->posY + (intval(self::HEIGHT / 2) + $nb_cases) * $way;


                //boucle de la zone a checker
    			for ($x = $x_i; $x != $x_e; $x += $way) {
    				for ($y = $y_i; $y != $y_e; $y += $way) {
                        //check des collisions avec les obstacles
    					if (in_array(array($x, $y), $obstacles))
    						return -1;

                        //check p1_ship
                        //il y que une donnees que on est entre son width min et max et si on est entr son height min et max
                        //foreach($p1_ship as $s)
                        //{
                        //	if (($nb = collision_vert($this, $s, $way)) != 0)
                        //		return $nb;
                        //}
    				}
    			}
    		} elseif ($this->orientation == 1 or $this->orientation == 3) {
    			$way = 1;
    			if ($this->orientation == 3)
    				$way = -1;

    			$x_i = $this->posX + (intval(self::HEIGHT / 2) * $way);
    			$y_i = $this->posY - intval(self::WIDTH / 2);

    			$x_e = $this->posX + (intval(self::HEIGHT / 2) + $nb_cases) * $way;
    			$y_e = $this->posY + intval(self::WIDTH / 2);

    			for ($y = $y_i; $y != $y_e; $y += $way) {
    				for ($x = $x_i; $x != $x_e; $x += $way) {
                        //check des collisions avec les obstacles
    					if (in_array(array($x, $y), $obstacles))
    						return -1;

                        //check p1_ship
                        //il y que une donnees que on est entre son width min et max et si on est entr son height min et max
                        //foreach($p1_ship as $s)
                        //{
                        //	if (($nb = collision_vert($this, $s, $way)) != 0)
                        //		return $nb;
                        //}
    				}
    			}

    		}
    	}

        //sortie de map
    	if ($x_e > MAP_WIDTH or $x_e < 0 or $y_e > MAP_HEIGHT or $y_e < 0)
    		return -1;
    }

    public function Turn($direction)
    {
    	if ($this->maniability == 0) {
    		if ($direction == "right") {
    			$next = $this->orientation++;
    			if ($next > 3)
    				$next = 0;
    			$this->orientation = $next;
    			return 0;
    		} elseif ($direction == "left") {
    			$next = $this->orientation--;
    			if ($next < 0)
    				$next = 3;
    			$this->orientation = $next;
    			return 0;
    		}
    		return -1;
    	} else
    	echo "Le vaisseau n'est pas immobile";
    	return -1;
    }

    public function AttributePoint($speed, $weapon, $shield)
    {
    	if ($speed < 0 or $weapon < 0 or $shield < 0)
    		return -1;
    	if ($speed + $weapon + $shield > self::MAX_PP)
    		return -1;
    	$this->pp_speed = $speed;
    	$this->pp_weapon = $weapon;
    	$this->pp_shield = $shield;

    	$this->pp -= ($speed + $weapon + $shield);
    	return 0;
    }

    public function Repair($repair)
    {
    	if ($this->maniability == 0) {
    		for ($i = 0; $i <= $repair; $i++) {
    			if (ThrowDices() == 6) {
    				$this->health++;
    				if ($this->health >= self::MAX_HEALTH)
    					$this->health = self::MAX_HEALTH;
    				return 0;
    			} else
    			echo "<br />echec reparation vaisseau<br />";
    			return 0;
    		}
    		return -1;
    	}
    	echo "Le vaisseau ne peut etre repare en cours de voyage";
    	return -1;
    }

    public function Shoot($weapon)
    {
    	$dice = ThrowDices();
    }

    public function ResetTurn()
    {
        //reset de tous les PP a chaque tour
    	$this->pp_speed = 0;
    	$this->pp_weapon = 0;
    	$this->pp_shield = 0;
    }
}
?>
