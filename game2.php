<?php

session_start();

require_once('Destroyer.class.php');

define('MAP_WIDTH', 100);
define('MAP_HEIGHT', 150);
define('OBSTACLES' , array(array(45, 54), array(23, 16), array(65, 87), array(55, 33)));

function ThrowDices()
{
	return (rand(1, 6));
}

$p1_ship = unserialize($_SESSION['p1_ship']);
$p2_ship = unserialize($_SESSION['p2_ship']);

?>

<html>
	<head>
		<title>42Game-<?php echo "Tour : P.".$_SESSION['turn']."";  ?></title>
	<head>
	<body>

<?php

//affichage des unites en cours
echo "<div id='area' style='border:5px black solid;border-radius:5px;width:500px;height:750px;'>";
foreach($p1_ship as $ship)
{
	if ($ship->getOrientation() == 0 or $ship->getOrientation() == 2)
		echo "<img src='blue_vert_".get_class($ship).".png' style='position:absolute;top:". $ship->getY()*5 ."px;left:". $ship->getX()*5 ."px;'/>";
	elseif ($ship->getOrientation() == 1 or $ship->getOrientation() == 3)
		echo "<img src='blue_hor_".get_class($ship).".png' style='position:absolute;top:". $ship->getY()*5 ."px;left:". $ship->getX()*5 ."px;'/>";
}
foreach($p2_ship as $ship)
{
	if ($ship->getOrientation() == 0 or $ship->getOrientation == 2)
		echo "<img src='red_vert_".get_class($ship).".png' style='position:absolute;bottom:". $ship->getY()*5 ."px;left:". $ship->getX()*5 ."px;'/>";
	elseif ($ship->getOrientation() == 1 or $ship->getOrientation() == 3)
    echo "<img src='red_hor_".get_class($ship).".png' style='position:absolute;bottom:". $ship->getY()*5 ."px;left:". $ship->getX()*5 ."px;'/>";
}
echo "</div>";

function Gameloop($p1_ship, $p2_ship)
{
	if (!empty($p1_ship) and empty($p2_ship))
	{
		echo "Joueur 1 gange";
		exit;
	}
	elseif (empty($p1_ship) and !empty($p2_ship))
	{
		echo "Joueur 2 gange";
		exit;
	}
	elseif (count($p1_ship) == 0 and count($p2_ship) == 0)
	{
		echo "Egalite";
		exit;
	}
	elseif (!empty($p1_ship) and !empty($p2_ship))
	{
        echo "pas vide";
		$ships = ($_SESSION['turn'] == 1) ? $p1_ship : $p2_ship;
        echo $_SESSION['turn']."<br />";
        echo $_SESSION['action']."<br />";
		foreach($ships as $ship)
		{
			if ($_SESSION['action'] == 'order')
			{
?>
			<form action='game2.php' method='get'>
			Attribute PP :<br />
			<label for='speed'>Speed</label><input type='text' id='speed' name='speed' required='required' /><br />
			<label for='weapon'>Weapon</label><input type='text' id='weapon' name='weapon' required='required' /><br />
			<label for='shield'>Shield</label><input type='text' id='shield' name='shield' required='required' /><br />
<?php
				if ($ship->getManiability() == 0)
				{
					echo "<label for='repair'>Repair</label><input type='text' id='repair' name='repair' required='required' /><br />";
				}
                echo "<input type='submit' value='Send'/>";
				echo "</form>";
                $repair = 0;
				if ($ship->getManiability() == 0 and $_GET['repair'] and gettype($_GET['repair']) == 'integer')
					$repair = intval(htmlspecialchars($_GET['repair']));
				elseif(!$_GET['repair'] or gettype(($_GET['repair']) != 'integer'))
					"Le champ repair est mal formate";
				if ($_GET['speed'] and $_GET['weapon'] and $_GET['shield'])
				{
					$speed = intval(htmlspecialchars($_GET['speed']));
					$weapon = intval(htmlspecialchars($_GET['weapon']));
					$shield = intval(htmlspecialchars($_GET['shield']));
                    $ship->AttributePoint($speed, $weapon, $shield);
                    $ship->Repair($repair);
                    $_SESSION['action'] = 'move';
				}
				else
					echo "Un ou plusieurs champs sont mal formates<br />";
                print($ship);
			}
			elseif ($_SESSION['action'] == 'move')
			{
?>
			<form action='game2.php' method='get'>
			<label for='nb_cases'>Number of case to move</label>
			<select name="nb_cases" id='nb_cases'>
<?php
				for($i = 0; $i != $ship->getSpeed() + $ship->getPP_speed(); $i++)
				{
					echo "<option>".$i."</option>";
				}
				echo "<select><br />";
				//free turn at the beginig
				if ($ship->getManiability() == 0)
				{
					echo "<label>Turn Before moving</label>";
					echo "<select name='turn' id='turn'>";
					echo "<option selected>none</option>";
					echo "<option>left</option>";
					echo "<option>right</option>";
				}
                echo "<input type='submit' value='Send'/>";
				echo "</form>";

				if ($_GET['turn'])
				{
					if ($_GET['turn'] == 'right')
						$ship->Turn('right');
					elseif ($_POST['turn'] == 'left')
						$ship->Turn('left');
				}
				if ($_GET['nb_cases'])
				{
					$ship->Move_ship($_GET['nb_cases']);
                    $_SESSION['action'] = 'shoot';
				}
                print($ship);
			}
			elseif ($_SESSION['action'] == 'shoot')
			{
				// passer au tour suivant que si tout a este poste
				$_SESSION['action'] = 'order';
				$_SESSION['turn'] = 2;
			}

			// on sauvegarde l'etat des vaisseaux avant de reboucler
			$_SESSION['p1_ship'] = $p1_ship;
			$_SESSION['p2_ship'] = $p2_ship;

		}
		$_SESSION['turn'] = ($_SESSION['turn'] == 1) ? 2 : 1;
	}
}

Gameloop($p1_ship, $p2_ship);
$_SESSION['p1_ship'] = serialize($p1_ship);
$_SESSION['p2_ship'] = serialize($p2_ship);

?>

	</body>
</html>

