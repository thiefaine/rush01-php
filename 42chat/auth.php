<?php

function auth($login, $passwd)
{
	if (!file_exists("../private/passwd"))
		return FALSE;
	$array = unserialize(file_get_contents("../private/passwd"));
	$hash = hash("whirlpool", $passwd);
	foreach ($array as $elem)
	{
		if ($elem[0] == $login and $elem[1] == $hash)
			return TRUE;
	}
	return FALSE;
}

?>
