<?php

session_start();

if ($_SESSION['loggued_on_user'] and $_SESSION['loggued_on_user'] != "")
{
	if (file_exists("../private/chat"))
	{
		date_default_timezone_set("EUROPE/PARIS");
		$handle = fopen("../private/chat", "r");
		if (flock($handle, LOCK_SH))
			$array = unserialize(file_get_contents("../private/chat"));
		flock($handle, LOCK_UN);
		fclose($handler);
		foreach ($array as $elem)
		{
			$date = getdate($elem[2]);
			echo "[".$date['hours'].":".$date['minutes']."] <b>".$elem[0]."</b>: ".$elem[1]."<br />\n";
		}
	}
}
?>
