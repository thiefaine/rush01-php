<?php

if ($_POST['submit'] == "OK" and $_POST['login'] != "" and $_POST['passwd'] != "")
{
	$array = array();
	$login = $_POST['login'];
	$pass = $_POST['passwd'];
	if (!file_exists("../private/"))
		mkdir("../private");
	if (file_exists("../private/passwd"))
	{
		$file = file_get_contents("../private/passwd");
		$array = unserialize($file);
		foreach ($array as $elem)
		{
			if ($elem[0] == $login)
			{
				echo "ERROR\n";
				exit;
			}
		}
	}
	$datas = array($login, hash("whirlpool", $pass));
	array_push($array, $datas);
	$array_serialize = serialize($array);
	file_put_contents("../private/passwd", $array_serialize);
	echo "OK\n";
	header('Location: index.html');
}
else
	echo "ERROR\n";

?>
