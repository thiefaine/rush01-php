<?php
if ($_POST['submit'] == "OK" and $_POST['login'] != "" and $_POST['oldpw'] != "" and $_POST['newpw'] != "")
{
	$exist = -1;
	$login = $_POST['login'];
	$oldpw = $_POST['oldpw'];
	$newpw = $_POST['newpw'];
	if (!file_exists("../private/passwd"))
	{
		echo "ERROR\n";
		exit;
	}
	$array = unserialize(file_get_contents("../private/passwd"));
	//check if the user exist and have the correct pw
	$i = 0;
	$hash_pw = hash("whirlpool", $oldpw);
	foreach($array as $elem)
	{
		if ($elem[0] == $login and $elem[1] == $hash_pw)
			$exist = $i;
		$i++;
	}
	if ($exist == -1)
	{
		echo "ERROR\n";
		exit;
	}
	//modify the pwd
	$array[$exist][1] = hash("whirlpool", $newpw);
	file_put_contents("../private/passwd", serialize($array));
	echo "OK\n";
	header('Location: index.html');
}
else
	echo "ERROR\n";

?>
