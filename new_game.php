<?php

session_start();

include 'var.php';

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$passwd = $GLOBALS['pass'];
$dbname = $GLOBALS['dbname'];
$game = $GLOBALS['game'];
$party_name = $GLOBALS['party_name'];

if ($_POST['name'] and $_POST['mode'] and $_POST['nb_joueur'])
{
	$name = htmlspecialchars($_POST['name']);
	$mode = htmlspecialchars($_POST['mode']);
	$nb_joueur = htmlspecialchars($_POST['nb_joueur']);
	$value = htmlspecialchars($_POST['value']);

	$conn = mysqli_connect($servername, $username, $passwd, $game);
	if (!$conn)
		die('Connection failed');
	$ret = mysqli_query($conn, "SELECT COUNT(*) FROM ".$party_name." WHERE name='".$name."'");
	$row = mysqli_fetch_array($ret);
	if ($row[0] == 0)
	{
		mysqli_query($conn, "INSERT INTO ".$party_name." (`id`, `name`, `mode`, `value`, `nb_joueur_max`, `nb_joueur`) VALUES (NULL, '".$name."', '".$mode."', '".$value."', '".$nb_joueur."', '0')");
		$sql_table = "CREATE TABLE IF NOT EXISTS game_".$name." (
			id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(30) NOT NULL,";
			for($i = 1; $i <= $nb_joueur; $i++)
			{
				$sql_table = $sql_table.$i."_player INT(6) NOT NULL, ";
				$sql_table = $sql_table.$i."_player_team VARCHAR(30) NOT NULL, ";
				$sql_table = $sql_table.$i."_player_ship TEXT NOT NULL, ";
			}
			$sql_table = $sql_table."turn INT(4) UNSIGNED DEFAULT 1 NOT NULL, action VARCHAR(30) DEFAULT 'order' NOT NULL)";

mysqli_query($conn, $sql_table);

mysqli_close($conn);
$_SESSION['partie'] = $name;
header('Location: creer_faction.php');
}
else
	header('Location: creer_partie.php?err=2');
}
else
	header('Location: creer_partie.php?err=1');
?>
