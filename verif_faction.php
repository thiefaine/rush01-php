<?php

session_start();

require_once 'Destroyer.class.php';
require_once 'Fregate.class.php';
require_once 'Cuirrasier.class.php';
include 'var.php';

$partie = $_SESSION['partie'];

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$passwd = $GLOBALS['pass'];
$dbname = $GLOBALS['dbname'];
$game = $GLOBALS['game'];
$party_name = $GLOBALS['party_name'];
$faction = htmlspecialchars($_POST['faction']);


$conn = mysqli_connect($servername, $username, $passwd, $game);
if (!$conn)
	die("Connection failed: " . mysqli_connect_error());
$ret = mysqli_query($conn, "SELECT * FROM ".$party_name." WHERE name='".$partie."'");
$row = mysqli_fetch_array($ret);

if ((ctype_digit($_POST['1_Orcs']) and ctype_digit($_POST['2_Orcs']) and ctype_digit($_POST['3_Orcs'])) or (ctype_digit($_POST['1_Imperiaux']) and ctype_digit($_POST['2_Imperiaux']) and ctype_digit($_POST['3_Imperiaux'])) or (ctype_digit($_POST['1_Morts-vivants']) and ctype_digit($_POST['2_Morts-vivants']) and ctype_digit($_POST['3_Morts-vivants'])))
{
	if ($_POST['1_Orcs'] or $_POST['2_Orcs'] or $_POST['3_Orcs'])
	{
		$ship_1 = $_POST['1_Orcs'];
		$ship_2 = $_POST['2_Orcs'];
		$ship_3 = $_POST['3_Orcs'];
	}
	else if ($_POST['1_Imperiaux'] or $_POST['2_Imperiaux'] or $_POST['3_Imperiaux'])
	{
		$ship_1 = $_POST['1_Imperiaux'];
		$ship_2 = $_POST['2_Imperiaux'];
		$ship_3 = $_POST['3_Imperiaux'];
	}
	else if ($_POST['1_Morts-vivants'] or $_POST['2_Morts-vivants'] or $_POST['3_Morts-vivants'])
	{
		$ship_1 = $_POST['1_Morts-vivants'];
		$ship_2 = $_POST['2_Morts-vivants'];
		$ship_3 = $_POST['3_Morts-vivants'];
	}
	$res = ($ship_1 * 100) + ($ship_2 * 200) + ($ship_3 * 300);
	if ($res == 0)
		header('Location: creer_faction.php?err=3');
	else if ($res <= $row['value'])
	{
		echo "ok";
		$game_partie = "game_".$partie."";
		$res = mysqli_query($conn, "SELECT * FROM ".$game_partie."");
		$row_res = mysqli_fetch_array($res);
		$flag = false;
	/*	for($i = 1; $i <= $row['nb_joueur_max']; $i++)
		{
			if ($row_res[$i."_player"] == $_SESSION['loggued_on_user'])
			{
				$flag = true;
				break ;
			}
		}*/
		if ($flag == false)
		{
			echo "ok";
			$array = array();
			for($i = 0; $i < $ship_1; $i++)
			{
				$str = 'destroyer_'.$i;
				$$str = new Destroyer();
				array_push($array, $$str);				
			}
			for($i = 0; $i < $ship_2; $i++)
			{
				$str = 'fregate_'.$i;
				$$str = new Fregate();
				array_push($array, $$str);				
			}
			for($i = 0; $i < $ship_3; $i++)
			{
				$str = 'cuirrassier_'.$i;
				$$str = new Cuirrasier();
				array_push($array, $$str);				
			}
			
			$seri = serialize($array);

			for($i = 1; $i <= $row['nb_joueur_max']; $i++)
			{
				if ($row_res[$i."_player"] == 0)
				{
					$nb_j = $row['nb_joueur'] + 1;
					mysqli_query($conn, "UPDATE ".$party_name." SET nb_joueur='".$nb_j."'");
					if (mysqli_num_rows($res) == 0)
					{			
						mysqli_query($conn, "INSERT INTO ".$game_partie." (`".$i."_player`, `".$i."_player_team`, `".$i."_player_ship`) VALUES ('".$_SESSION['loggued_on_user']."', '".$faction."', '".$seri."')");
					}
					else
					{
						mysqli_query($conn, "UPDATE ".$game_partie." SET ".$i."_player='".$_SESSION['loggued_on_user']."'");
						mysqli_query($conn, "UPDATE ".$game_partie." SET ".$i."_player_team='".$faction."'");
						mysqli_query($conn, "UPDATE ".$game_partie." SET ".$i."_player_ship='".$seri."'");
					}
					$flag = true;
					break ;
				}
			}
		}
		if ($flag == true)
			header('Location: game.php');
		else
			header('Location: rejoindre.php?err=1');
	}
	else
		header('Location: creer_faction.php?err=2');
}
else
	header('Location: creer_faction.php?err=1');
?>