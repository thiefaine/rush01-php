<?php

interface IShip
{
	function TakeDamage($degats);

	function Shoot($weapon);

	function Move_ship($nb_cases, $obstacles);

	function Turn($direction);

	function AttributePoint($speed, $weapon, $shield);

	function Repair($repair);

	function ResetTurn();

	function __toString();
}

?>
