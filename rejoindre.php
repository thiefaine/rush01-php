<?php

include 'var.php';

session_start();

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$passwd = $GLOBALS['pass'];
$dbname = $GLOBALS['dbname'];
$game = $GLOBALS['game'];



if ($_SESSION['loggued_on_user'])
{
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<meta charset="utf-8" />
		<link rel="stylesheet" href="rush.css" />
		<title>42</title>
	</head>
	<body>
		<?php
		include('head.php');
		echo "<h1 class='table'>Les Parties en cours....</h1>";
		if ($_GET['err'] == 1) { 
			echo "<strong>Partie complete</strong>"; } 
		$conn = mysqli_connect($servername, $username, $passwd, $game);
		if (!$conn)
			die("Connection failed: " . mysqli_connect_error());
		$count = mysqli_query($conn, "SELECT * FROM ".$GLOBALS['party_name']."");
		?>
		<table id="customers">
			<thead>
				<tr>
					<th>Nom</th>
					<th>Mode</th>
					<th>Value</th>
					<th>Joueurs max</th>
					<th>Joueurs online</th>
					<th>Choice</th>
				</tr>
			</thead>
			<tbody>
				<?php
				while ($row = mysqli_fetch_array($count))
				{
					echo "<tr>";
					echo "<td class='alt'>".$row['name']."</td>";
					echo "<td>".$row['mode']."</td>";
					echo "<td>".$row['value']."</td>";
					echo "<td>".$row['nb_joueur_max']."</td>";
					echo "<td>".$row['nb_joueur']."</td>";
					echo "<td>";
					if ($row['nb_joueur'] < $row['nb_joueur_max'])
					{
						echo "<form action='creer_faction.php' method='post'>";
						echo "<input style='width:60px' type=submit value='OK'></input>";
						echo "<input type=hidden name='name' value=".$row['name']." </input>";
						echo "</form>";
					}
					else
					{
						echo "<span>Partie pleine</span>";						
					}
					echo "</td>";
					echo "</tr>";
				}
				echo "</tbody></table>";
				echo "<div><br /><br /><br /><br /></div>";
				include 'footer.php';
				?>
			</body>
			</html>
			<?php }
			else 
				header('Location: index.php');
			?>
