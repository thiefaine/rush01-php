<?php
session_start();

include 'var.php';

$servername = $GLOBALS['server'];
$username = $GLOBALS['user'];
$password = $GLOBALS['pass'];
$dbname = $GLOBALS['dbname'];

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="rush.css" />
	<title>42</title>
</head>
<body>
<?php
include 'head.php';

if ($_POST['login'] and $_POST['passwd'])
{
	$login = htmlspecialchars($_POST['login']);
	$passwd = htmlspecialchars($_POST['passwd']);
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if (mysqli_connect_errno())
		echo "Failed to connect to MySQL: " . mysqli_connect_error();
	$res = mysqli_query($conn,"SELECT * FROM ".$GLOBALS['guest']." WHERE login='".$login."'");
	$row = mysqli_fetch_array($res);
	if (mysqli_num_rows($res))
	{
		if (hash("whirlpool", $passwd) == $row['passwd'])
		{
			$_SESSION['loggued_on_user'] = $row['id'];
			$_SESSION['member'] = $row['member'];
			$_SESSION['panier'] = array();
			header ('Location: index.php');
			mysqli_close($conn);
		}
		else
			echo "<p class='error'>Password incorrect</p>";
	}
	else
		echo "<p class='error'>Username inexistant</p>";
	mysqli_close($conn);
}

?>
	<form class="login" action="login.php" method="post">
		<label for='login'>Identifiant : </label><input id='login' type="text" name="login" value="" required="required" />
		<br />
		<label for='passwd_log'>Mot de passe : </label><input id='passwd_log' type="password" name="passwd" value="" required="required" />
		<br />
		<input class='sub' type="submit" name="submit" value="OK" />
		<br />
	</form>
<?php
include 'footer.php'
?>
</body></html>
