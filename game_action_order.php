<?php
session_start();
require_once 'Destroyer.class.php';
require_once 'Fregate.class.php';
require_once 'Cuirrasier.class.php';
include('var.php');

if ($_POST['speed'] and $_POST['shield'] and $_POST['weapon'] and $_POST['turn'])
{
	$speed = htmlspecialchars($_POST['speed']);
	$shield = htmlspecialchars($_POST['shield']);
	$weapon = htmlspecialchars($_POST['weapon']);
	$turn = htmlspecialchars($_POST['turn']);

	$name = $_SESSION['partie'];

	$servername = $GLOBALS['server'];
	$username = $GLOBALS['user'];
	$passwd = $GLOBALS['pass'];
	$game = $GLOBALS['game'];
	$party_name = $GLOBALS['party_name'];
	$game_partie = "game_".$name."";

	$conn = mysqli_connect($servername, $username, $passwd, $game);
	if (!$conn)
		die('Connection failed');
	$res = mysqli_query($conn, "SELECT * FROM game_".$name."");
	$row = mysqli_fetch_array($res);
	$ship = unserialize($row[$turn."_player_ship"]);
	$ship[$_SESSION['i_ship']]->AttributePoint($speed, $weapon, $shield);
	mysqli_query($conn, "UPDATE ".$game_partie." SET action='move'");
	$obj = serialize($ship);
	mysqli_query($conn, "UPDATE ".$game_partie." SET ".$turn."_player_ship='".$obj."'");
	header('Location: game.php');
}
?>