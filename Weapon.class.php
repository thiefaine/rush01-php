<?php

class Weapon
{
	const SHORT = 4;
	const MIDDLE = 5;
	const LONG = 6;

	private $charges = 0;
	private $short_range = 0;
	private $middle_range = 0;
	private $long_range = 0;

	public function __construct(array $kwarg)
	{
		if (!$kwarg['charges'] and !$kwarg['array_range'] and count($kwarg['array_range']) == 3)
		{
			echo "il manque des arguments definissant la classe";
			return -1;
		}
		else
		{
			$this->charges = $kwarg['charges'];
			$this->short_range = $kwarg['array_range'][0];
			$this->middle_range = $kwarg['array_range'][1];
			$this->long_range = $kwarg['array_range'][2];
		}
		return 0;
	}

	public function __destruct()
	{
	}

	public function animation($posX_ship, $posY_ship)
	{
	}
}

?>
