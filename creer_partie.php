<?php

session_start();

$_SESSION['partie'] = "";
if ($_SESSION['loggued_on_user'])
{

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<link rel="stylesheet" href="rush.css" />
	<title>42</title>
</head>
<body>
<?php

	include 'head.php';

	if ($_GET['err'] == 1) { 
		echo "<strong>Un ou plusieurs champs ne sont pas valides</strong>"; }
	else if ($_GET['err'] == 2) {
		echo "<strong>Le nom de la partie existe deja, selectionner en un autre</strong>"; }
?>
				<h1>Creer une nouvelle partie</h1>

				<form action='new_game.php' method='post'>
					<label for='name'>Nom de la partie : </label><input type='text' id='name' name='name'/>
					<br />
					<label for='mode'>Mode de jeu : </label>
					<select name='mode' id='mode'>
						<option selected>Melee</option>
						<option>Team</option>
					</select>
					<br />
					<label for='nb_joueur'>Nombre de joueurs : </label>
					<select name='nb_joueur' id='nb_joueur'>
						<option>2</option>
						<option>3</option>
						<option>4</option>
					</select>
					<br />
					<label for='value'>Valeur de flotte : </label>
					<select name='value' id='value'>
						<option>500</option>
						<option>1000</option>
						<option>1500</option>
					</select>
					<br />
					<input type='submit' value='Creer'>
				</form>
		</body>
</html>
<?php
}
else
	header('Location: index.php');
include 'footer.php';
?>
