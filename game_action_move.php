<?php
session_start();
require_once 'Destroyer.class.php';
require_once 'Fregate.class.php';
require_once 'Cuirrasier.class.php';
include('var.php');

if ($_POST['nb_cases'])
{
	$nb_cases = htmlspecialchars($_POST['nb_cases']);
	$turn = htmlspecialchars($_POST['turn']);

	$name = $_SESSION['partie'];

	$servername = $GLOBALS['server'];
	$username = $GLOBALS['user'];
	$passwd = $GLOBALS['pass'];
	$game = $GLOBALS['game'];
	$party_name = $GLOBALS['party_name'];
	$game_partie = "game_".$name."";

	$conn = mysqli_connect($servername, $username, $passwd, $game);
	if (!$conn)
		die('Connection failed');
	$res = mysqli_query($conn, "SELECT * FROM game_".$name."");
	$row = mysqli_fetch_array($res);
	$ship = unserialize($row[$turn."_player_ship"]);
	$ship[$_SESSION['i_ship']]->Move_ship($nb_cases, $GLOBALS['OBSTACLES']);
	mysqli_query($conn, "UPDATE ".$game_partie." SET action='shoot'");
	$obj = serialize($ship);
	mysqli_query($conn, "UPDATE ".$game_partie." SET ".$turn."_player_ship='".$obj."'");
	header('Location: game.php');
}
//else
	//header('Location: game.php');	
?>