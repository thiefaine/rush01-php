Lance---------------------------------

Arme de destruction, prevue pour l'erradication pure et simple.

Elle se porte principalement sur les gros navires dont elle par defaut

sur tous les cuirrassiers.

On peut preciser sa charge et ses differentes portees dans le constructeur:

(charge, array(portee courte, portee moyenne, portee longue))

Ses caracteristiaues par defaut sont :

	charge : 0
	courte : 30
	moyenne : 60
	longue : 90

Zone d'effet : ligne droite devant le vaisseau

--------------------------------------
